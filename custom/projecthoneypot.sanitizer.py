from abusehelper.core import events

import genericsanitizer
import sanitizer
import socket

class ProjectHoneypotSanitizer(genericsanitizer.GenericSanitizer):

    ### EVENT (Raw) Data Source
    
    raw_triggertime          = "source time"                        # Event Trigger Time (from source)
            
    raw_eventdesc            = "description"                        # Event Description                  

    raw_ip                   = "ip"                                 # IP Address     


    ### Source Name
    def sanitize_sourcename(self):
        self.sanitized_event.update(self.sanitized_sourcename, ["ProjectHoneypot"])
        return self.data_validation(self.sanitized_sourcename)
        
    ### Source Description
    def sanitize_sourcedesc(self):
        self.sanitized_event.update(self.sanitized_sourcedesc, ["Honeypots"])
        return self.data_validation(self.sanitized_sourcedesc)        

    ### Incident Class
    def sanitize_incidentclass(self):
        self.sanitized_event.update(self.sanitized_incidentclass, ["Abusive Content"])
        return self.data_validation(self.sanitized_incidentclass)

    ### Incident Type
    def sanitize_incidenttype(self):
        self.sanitized_event.update(self.sanitized_incidenttype, ["SPAM"])        
        return self.data_validation(self.sanitized_incidenttype)
        
    ### Event Type              
    def sanitize_eventtype(self):
        self.sanitized_event.update(self.sanitized_eventtype, ["Emails Flood"])        
        return self.data_validation(self.sanitized_eventtype) 
                
    ### Event Notes            
    def sanitize_eventnotes(self):
        event_notes = "["
        if self.raw_event.contains("count"): 
            event_notes += "count:" + str(self.raw_event.value("count")) + " "
        if self.raw_event.contains("description"): 
            event_notes += str(self.raw_event.value("description url")) + " "
        event_notes += "]"            
        self.raw_event.update(self.raw_eventnotes, [event_notes])                    
        self.data_update(raw_param = self.raw_eventnotes, sanitized_param = self.sanitized_eventnotes)           
            
        
if __name__ == "__main__":
    ProjectHoneypotSanitizer.from_command_line().execute()
