from abusehelper.core import events
import genericsanitizer
import sanitizer
import socket


class AbuseCHSanitizer(genericsanitizer.GenericSanitizer):

    ### EVENT (Raw) Data Source

    raw_sourcedesc           = "type"                   # Feed Specification (ex.: zeus, drone)

    raw_triggertime          = "time"                   # Event Trigger Time (from source)
    raw_status               = "status"                 # Event Status (ex.: online or offline | default value is online)

    raw_incidenttype         = "type"                   # Incident Type (based on European CSIRT Network Taxonomy)
    raw_eventtype            = "malware"                # Event Type (specifications based on European CSIRT Network Taxonomy)
    raw_eventdesc            = "level"                  # Event Description                  

    raw_ip                   = "ip"                     # IP Address
    raw_asncode              = "asncode"                # ASN (Autonomous System Number)
    raw_host                 = "host"                   # Host    
    raw_url                  = "url"                    # URL    
    

    ### Source Name
    def sanitize_sourcename(self):
        return self.data_sanitized_insert(self.sanitized_sourcename, "Abuse.ch")


    ### URL
    def sanitize_url(self):
        res = None      
        if not self.raw_event.value(self.raw_incidenttype) == "c&c":
            if not res:
                res = self.data_bypass(raw_param = self.raw_url, sanitized_param = self.sanitized_url)
            
            if not res:        
                res = self.data_bypass(raw_param = "spyeye binaryurl", sanitized_param = self.sanitized_url)
                        
            if not res:    
                res = self.data_bypass(raw_param = "spyeye configurl", sanitized_param = self.sanitized_url)    

            if not res:    
                res = self.data_bypass(raw_param = "spyeye dropurl", sanitized_param = self.sanitized_url)                      
                
                    
    ### Event Description              
    def sanitize_eventdesc(self):
        if self.raw_event.contains(self.raw_eventdesc):
            if self.raw_event.value(self.raw_eventdesc) != "unknown":
                self.data_bypass(raw_param = self.raw_eventdesc, sanitized_param = self.sanitized_eventdesc)


    ### Event Notes            
    def sanitize_eventnotes(self):
        event_notes = "["
        if self.raw_event.contains("slb"): 
            event_notes += str(self.raw_event.value("slb")) + " "
        if self.raw_event.contains("more info"): 
            event_notes += str(self.raw_event.value("more info")) + " "
        event_notes += "]"            
        self.data_raw_insert(event_notes, self.raw_eventnotes)
        self.data_bypass(raw_param = self.raw_eventnotes, sanitized_param = self.sanitized_eventnotes)            
                   
                   
    ### Incident Class
    def sanitize_incidentclass(self):
        return self.data_sanitized_insert(self.sanitized_incidentclass, "Malware")


    ### Incident Type
    def sanitize_incidenttype(self):
        incidenttype = str(self.raw_event.value(self.raw_incidenttype))
        if incidenttype == "c&c":       
            return self.data_sanitized_insert(self.sanitized_incidenttype, "C&C")            
        else:
            return self.data_sanitized_insert(self.sanitized_incidenttype, "Distribution")            
        

if __name__ == "__main__":
    AbuseCHSanitizer.from_command_line().execute()
