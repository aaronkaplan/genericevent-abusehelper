import sys # to remove

from abusehelper.contrib.enrichment.enrichment import EventEnrichment
from abusehelper.contrib.enrichment.extraenrichment import enrich_basic_info
from abusehelper.core import events
import sanitizer
import unicodedata
import time
import socket
import hashlib
from datetime import timedelta
from dateutil import parser 




def normalize(text):
    text = "".join(text)
    try:
        new_text = str(text.encode("utf-8"))
    except UnicodeDecodeError:
        new_text = text.decode("utf-8")
    return new_text 

def get_timezones():    
    tz_str = '''-12 Y
        -11 X NUT SST
        -10 W CKT HAST HST TAHT TKT
        -9 V AKST GAMT GIT HADT HNY
        -8 U AKDT CIST HAY HNP PST PT
        -7 T HAP HNR MST PDT
        -6 S CST EAST GALT HAR HNC MDT
        -5 R CDT COT EASST ECT EST ET HAC HNE PET
        -4 Q AST BOT CLT COST EDT FKT GYT HAE HNA PYT
        -3 P ADT ART BRT CLST FKST GFT HAA PMST PYST SRT UYT WGT
        -2 O BRST FNT PMDT UYST WGST
        -1 N AZOT CVT EGT
        0 Z EGST GMT UTC WET WT
        1 A CET DFT WAT WEDT WEST
        2 B CAT CEDT CEST EET SAST WAST
        3 C EAT EEDT EEST IDT MSK
        4 D AMT AZT GET GST KUYT MSD MUT RET SAMT SCT
        5 E AMST AQTT AZST HMT MAWT MVT PKT TFT TJT TMT UZT YEKT
        6 F ALMT BIOT BTT IOT KGT NOVT OMST YEKST
        7 G CXT DAVT HOVT ICT KRAT NOVST OMSST THA WIB
        8 H ACT AWST BDT BNT CAST HKT IRKT KRAST MYT PHT SGT ULAT WITA WST
        9 I AWDT IRKST JST KST PWT TLT WDT WIT YAKT
        10 K AEST ChST PGT VLAT YAKST YAPT
        11 L AEDT LHDT MAGT NCT PONT SBT VLAST VUT
        12 M ANAST ANAT FJT GILT MAGST MHT NZST PETST PETT TVT WFT
        13 FJST NZDT
        11.5 NFT
        10.5 ACDT LHST
        9.5 ACST
        6.5 CCT MMT
        5.75 NPT
        5.5 SLT
        4.5 AFT IRDT
        3.5 IRST
        -2.5 HAT NDT
        -3.5 HNT NST NT
        -4.5 HLV VET
        -9.5 MART MIT'''

    tzd = {}
    for tz_descr in map(str.split, tz_str.split('\n')):
        x = open('./cenas.txt', 'a')
        x.write(str(tz_descr) + '\n')
        x.close()
        tz_offset = int(float(tz_descr[0]) * 3600)
        for tz_code in tz_descr[1:]:
            tzd[tz_code] = tz_offset
            
    return tzd       
 
def time_convert(raw_time, tzd):
    import pytz

    aux_time = None

                     
    if not aux_time:              
        try:
            aux_time = parser.parse(raw_time, tzinfos=tzd).astimezone(pytz.utc)                                                                                            
        except ValueError as e:
            '''Debug - print "(ERROR) Normal conversion failed. RawTime: %r, Exception: %r", raw_time, e '''


    if not aux_time:                       
        try:
            aux_raw_time = raw_time + " UTC"
            aux_time = parser.parse(aux_raw_time, tzinfos=tzd).astimezone(pytz.utc)
        except ValueError as e:
            '''Debug - print "(ERROR) UTC conversion failed. RawTime: %r, Exception: %r", raw_time, e '''
        
        
    if not aux_time:                       
        try:      
            aux_time = datetime.strptime(raw_time, "%Y-%m-%d")
        except ValueError as e:
            '''Debug - print "(ERROR) Date to Datetime failed. RawTime: %r, Exception: %r", raw_time, e '''


    if aux_time:
        formated_time = aux_time.strftime('%Y-%m-%d %H:%M:%S UTC')
    else:
        self.log.error("Time Formating fail. Pre-FormatedTime: %r, Exception: %r", aux_time, e)
        formated_time = None

    
    return formated_time    










class GenericSanitizer(sanitizer.Sanitizer):

    raw_event                = None             # Event from source
    sanitized_event          = None             # Sanitized event for deployment


    ### EVENT (Sanitized)
    
    sanitized_sourcename     = "source_name"    # Feed Source (ex.: Abusech, DShield)
    sanitized_sourcedesc     = "source_desc"    # Feed Specification (ex.: zeus, drone)

    sanitized_triggertime    = "trigger_time"   # Event Trigger Time (from source)
    sanitized_registertime   = "register_time"  # Event Register Time (datetime.now from abusehelper system)
    sanitized_status         = "status"         # Event Status (ex.: online or offline or unknown | default value is online)
            
    sanitized_incidentclass  = "incident_class" # Incident Class (based on European CSIRT Network Taxonomy)
    sanitized_incidenttype   = "incident_type"  # Incident Type (based on European CSIRT Network Taxonomy)
    sanitized_eventtype      = "event_type"     # Event Type (specifications based on European CSIRT Network Taxonomy)
    sanitized_eventdesc      = "event_descr"    # Event Description
    sanitized_eventnotes     = "event_notes"    # Event Notes (all information from source that dont fill any generic field)
    sanitized_eventhash      = "event_hash"     # EventHash

    sanitized_ip             = "ip"             # IP Address
    sanitized_netblock       = "netblock"       # Netblock    
    sanitized_bgp            = "bgp"            # BGP Prefix        
    sanitized_asncode        = "asn_code"       # ASN (Autonomous System Number)
    sanitized_asnname        = "asn_name"       # ASN (Autonomous System Number) Name
    sanitized_countrycode    = "country_code"   # Country Code
    sanitized_host           = "host"           # Host
    sanitized_url            = "url"            # URL
    
    ### EVENT (Raw)    
    
    raw_sourcename           = "default_raw_sourcename"             # Feed Source (ex.: Abusech, DShield)
    raw_sourcedesc           = "default_raw_sourcedesc"             # Feed Specification (ex.: zeus, drone)

    raw_triggertime          = "default_raw_triggertime"            # Event Trigger Time (from source)
    raw_registertime         = "default_raw_registertime"           # Event Register Time (datetime.now from abusehelper system)
    raw_status               = "default_raw_status"                 # Event Status (ex.: online or offline | default value is online)
            
    raw_incidentclass        = "default_raw_incidentclass"          # Incident Class (based on European CSIRT Network Taxonomy)
    raw_incidenttype         = "default_raw_incidenttype"           # Incident Type (based on European CSIRT Network Taxonomy)
    raw_eventtype            = "default_raw_eventtype"              # Event Type (specifications based on European CSIRT Network Taxonomy)
    raw_eventdesc            = "default_raw_eventdesc"              # Event Description                  
    raw_eventnotes           = "default_raw_eventnotes"             # Event Notes (all information from source that dont fill any generic field)

    raw_ip                   = "default_raw_ip"                     # IP Address
    raw_netblock             = "default_raw_netblock"               # Netblock    
    raw_bgp                  = "default_raw_bgp"                    # BGP Prefix        
    raw_asncode              = "default_raw_asncode"                # ASN (Autonomous System Number)
    raw_asnname              = "default_raw_asnname"                # ASN (Autonomous System Number) Name    
    raw_countrycode          = "default_raw_countrycode"            # Country Code
    raw_host                 = "default_raw_host"                   # Host
    raw_url                  = "default_raw_url"                    # URL        


    ### Sanitize
    def sanitize(self, event):
    
        self.sanitized_event = events.Event()
        self.raw_event = event
             
                 
                 
        '''     *** SANITIZE INFORMATION ***      '''                           
        
        self.sanitize_sourcename()
        self.sanitize_sourcedesc()
        
        self.sanitize_triggertime()
        self.sanitize_registertime()        
        self.sanitize_status()
        
        self.sanitize_incidentclass()
        self.sanitize_incidenttype()
        self.sanitize_eventtype()
        self.sanitize_eventdesc()
        self.sanitize_eventnotes()        
       
        self.sanitize_ip()    
        self.sanitize_netblock()          
        self.sanitize_bgp()          
        self.sanitize_asn()   
        self.sanitize_asnname()           
        self.sanitize_countrycode()         
        self.sanitize_host() 
        self.sanitize_url()                                                                                                 



        '''     *** ENRICHMENT ***      '''
        
        status = None
        if self.sanitized_event.contains(self.sanitized_status):    
            status = self.sanitized_event.value(self.sanitized_status)

        if status == "online":
            self.get_basic_info()     
            self.get_extra_info()


        '''     *** DATA VALIDATION ***      '''
        
        self.data_validation(self.sanitized_sourcename)
        self.data_validation(self.sanitized_sourcedesc)
        self.data_validation(self.sanitized_triggertime)
        self.data_validation(self.sanitized_registertime)                        
        self.data_validation(self.sanitized_status)
        self.data_validation(self.sanitized_incidentclass)
        self.data_validation(self.sanitized_incidenttype)
        self.data_validation(self.sanitized_eventtype)
        self.data_validation(self.sanitized_eventdesc)        

        self.data_validation(self.sanitized_eventnotes)                    
        self.data_validation(self.sanitized_ip)
        self.data_validation(self.sanitized_netblock)
        self.data_validation(self.sanitized_bgp)        
        self.data_validation(self.sanitized_asncode)
        self.data_validation(self.sanitized_asnname)        
        self.data_validation(self.sanitized_countrycode)       
        self.data_validation(self.sanitized_host)
        self.data_validation(self.sanitized_url)
        
        self.create_hashevent()      

        yield self.sanitized_event    
    
    
    '''
        SOURCE
    '''          
    ### Source Name
    def sanitize_sourcename(self):
        self.data_bypass(raw_param = self.raw_sourcename, sanitized_param = self.sanitized_sourcename)
        
    ### Source Description
    def sanitize_sourcedesc(self):
        self.data_bypass(raw_param = self.raw_sourcedesc, sanitized_param = self.sanitized_sourcedesc)
        

    '''
        TIME
    '''              
    ### Trigger Time        
    def sanitize_triggertime(self):    
        import pytz        
        tzd = get_timezones()  
        raw_time = self.raw_event.value(self.raw_triggertime)
        
        formated_time = time_convert(raw_time, tzd)
          
        if formated_time:                       
            self.data_raw_insert(self.raw_triggertime, formated_time)
            self.data_bypass(raw_param = self.raw_triggertime, sanitized_param = self.sanitized_triggertime)              


    ### Register Time
    def sanitize_registertime(self):    
        from datetime import datetime
        datenow = datetime.utcnow()       
        register_time = datenow.strftime('%Y-%m-%d %H:%M:%S UTC')                

        self.data_raw_insert(self.raw_registertime, register_time)
        self.data_bypass(raw_param = self.raw_registertime, sanitized_param = self.sanitized_registertime)      

        
    ### Event Status
    def sanitize_status(self):
        self.data_bypass(raw_param = self.raw_status, sanitized_param = self.sanitized_status)                

        
    '''
        INCIDENT CLASSIFICATION
    '''                  
    ### Incident Class
    def sanitize_incidentclass(self):
        self.data_bypass(raw_param = self.raw_incidentclass, sanitized_param = self.sanitized_incidentclass)    

    ### Incident Type
    def sanitize_incidenttype(self):
        self.data_bypass(raw_param = self.raw_incidenttype, sanitized_param = self.sanitized_incidenttype)          

    ### Event Type
    def sanitize_eventtype(self):
        self.data_bypass(raw_param = self.raw_eventtype, sanitized_param = self.sanitized_eventtype)    

    ### Event Description
    def sanitize_eventdesc(self):
        self.data_bypass(raw_param = self.raw_eventdesc, sanitized_param = self.sanitized_eventdesc)       
        
    ### Event Notes
    def sanitize_eventnotes(self):
        self.data_bypass(raw_param = self.raw_eventnotes, sanitized_param = self.sanitized_eventnotes)          
       

    '''
        TECHNICAL INFO
    '''             
    ### IP
    def sanitize_ip(self):
        self.data_bypass(raw_param = self.raw_ip, sanitized_param = self.sanitized_ip, extra_sanitize = sanitizer.ip)  
        
    ### Netblock
    def sanitize_netblock(self):
        self.data_bypass(raw_param = self.raw_netblock, sanitized_param = self.sanitized_netblock)    
        
    ### Netblock
    def sanitize_bgp(self):
        self.data_bypass(raw_param = self.raw_bgp, sanitized_param = self.sanitized_bgp)              
        
    ### ASN
    def sanitize_asn(self):
        self.data_bypass(raw_param = self.raw_asncode, sanitized_param = self.sanitized_asncode)     
        
    ### ASN Name
    def sanitize_asnname(self):
        self.data_bypass(raw_param = self.raw_asnname, sanitized_param = self.sanitized_asnname)                 
        
    ### Country Code
    def sanitize_countrycode(self):
        self.data_bypass(raw_param = self.raw_countrycode, sanitized_param = self.sanitized_countrycode)                           
        
    ### Host
    def sanitize_host(self):
        self.data_bypass(raw_param = self.raw_host, sanitized_param = self.sanitized_host)

    ### URL
    def sanitize_url(self):
        self.data_bypass(raw_param = self.raw_url, sanitized_param = self.sanitized_url)
                
    ### Hash Event
    def create_hashevent(self):
        string = str(self.sanitized_event.value(self.sanitized_sourcename)) +    str(self.sanitized_event.value(self.sanitized_sourcedesc)) +     str(self.sanitized_event.value(self.sanitized_triggertime))
        string += str(self.sanitized_event.value(self.sanitized_status)) +       str(self.sanitized_event.value(self.sanitized_incidentclass)) +  str(self.sanitized_event.value(self.sanitized_incidenttype))
        string += str(self.sanitized_event.value(self.sanitized_eventtype)) +    str(self.sanitized_event.value(self.sanitized_eventdesc)) +      str(self.sanitized_event.value(self.sanitized_eventnotes))
        string += str(self.sanitized_event.value(self.sanitized_ip)) +           str(self.sanitized_event.value(self.sanitized_netblock)) +       str(self.sanitized_event.value(self.sanitized_asncode))
        string += str(self.sanitized_event.value(self.sanitized_countrycode)) +  str(self.sanitized_event.value(self.sanitized_host)) +         str(self.sanitized_event.value(self.sanitized_url))

        eventhash = hashlib.sha256(string).hexdigest()            
        self.data_sanitized_insert(self.sanitized_eventhash, eventhash)   



    ### Data Event Sanitized Insert            
    def data_sanitized_insert(self, sanitized_param, value):
        self.sanitized_event.update(sanitized_param, [normalize(value)])
        return True    
        
    ### Data Event Raw Insert            
    def data_raw_insert(self, raw_param, value):
        self.raw_event.update(raw_param, [normalize(value)])
        return True                

    ### Data Bypass
    def data_bypass(self, raw_param, sanitized_param, extra_sanitize = None, force = False):
        if self.raw_event.contains(raw_param):
            if self.sanitized_event.contains(sanitized_param) or force:            
                return False
            else:
                self.data_sanitized_insert(sanitized_param, self.raw_event.values(raw_param, extra_sanitize)))                
                return True
        else:
            return False
            
    ### Data Validation
    def data_validation(self, sanitized_param):
        if not self.sanitized_event.contains(sanitized_param):
            self.sanitized_event.update(sanitized_param, ["N/A"])
            return True
        return False    
        
        
        


    # Get IP and Host if not defined
    def get_basic_info(self):
        ip     = None
        host   = None
        url    = None
    
        if self.sanitized_event.contains(self.sanitized_ip):    
            ip      = self.sanitized_event.value(self.sanitized_ip)
        if self.sanitized_event.contains(self.sanitized_host):
            host    = self.sanitized_event.value(self.sanitized_host)
        if self.sanitized_event.contains(self.sanitized_url):            
            url     = self.sanitized_event.value(self.sanitized_url)

        if not ip or not host:
            ip, host = enrich_basic_info(ip, host, url)
            
            if ip:
                self.data_sanitized_insert(self.sanitized_ip, ip)
            if host:    
                self.data_sanitized_insert(self.sanitized_host, host)


    # Get Netblock, ASN, ASName, Country and BGP if not defined
    def get_extra_info(self):
        ip           = None
        netblock     = None
        bgp          = None        
        asn          = None
        country      = None       
        asnname      = None              
    
        if self.sanitized_event.contains(self.sanitized_ip):    
            ip = self.sanitized_event.value(self.sanitized_ip)
                
        if ip:        
            if self.sanitized_event.contains(self.sanitized_netblock):    
                netblock    = self.sanitized_event.value(self.sanitized_netblock)
            if self.sanitized_event.contains(self.sanitized_asncode):    
                asn         = self.sanitized_event.value(self.sanitized_asncode)
            if self.sanitized_event.contains(self.sanitized_asnname):    
                asnname     = self.sanitized_event.value(self.sanitized_asnname)
            if self.sanitized_event.contains(self.sanitized_countrycode):    
                country     = self.sanitized_event.value(self.sanitized_countrycode)
        
            if netblock is None or asn is None or country is None:
                event_enrichment = EventEnrichment() 
                ai = event_enrichment.lookup(ip)                                
            
                if ai:    
                    country = ai.getCountryCode() 
                    asn = ai.getASN()            
                    asnname = ai.getASName()            
                    netblock = ai.getIPRange()                                      
                    bgp = ai.getBGP()                  
                
                    if netblock:
                        netblock = "-".join(netblock)
                        self.data_sanitized_insert(self.sanitized_netblock, netblock)
                    if asn:
                        self.data_sanitized_insert(self.sanitized_asncode, asn)                        
                    if asnname:
                        self.data_sanitized_insert(self.sanitized_asnname, asnname)
                    if country: 
                        self.data_sanitized_insert(self.sanitized_countrycode, country)
                    if bgp: 
                        self.data_sanitized_insert(self.sanitized_bgp, bgp)
                                                  
              
            if country is None or asn is None or netblock is None and bgp is None:
                self.log.info("Event without all information - IP: %r | IPrange: %r | Country: %r | ASN: %r | ASName: %r | BGP: %r", str(ip), str(netblock), str(country), str(asn), str(asnname), str(bgp))     
                                                                
