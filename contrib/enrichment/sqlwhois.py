import commands
import re
import time
import string
import pgdb

from abusehelper.contrib.enrichment.abuseinfo import AbuseInfo

class SQLWhois():

    connection = None
    #SQL Queries
    __select_all = """     SELECT IPRanges.range, 
                                  IPRanges.netname, 
                                  IPRanges.asn,
                                  IPRanges.acronym,
                                  ASN.asname, 
                                  ASN.email,
                                  ASN.country_code, 
                                  Country.name 
                            FROM Country, ASN, IPRanges 
                            WHERE IPRanges.range >>= %s;
                        """
    __insert_asn__ = "INSERT INTO ASN (asn, asname, country_code, email) VALUES ( %d, %s , %s , %s );"
    __insert_organisation__ = "INSERT INTO Organisation (acronym, name, asn) VALUES ( %s, %s, %d );"
    __insert_ipranges__ = "INSERT INTO IPRanges (range, asn, netname, acronym, email) VALUES ( %s, %d, %s, %s, %s );"
    __insert_contact__ = "INSERT INTO Contact (fname, lname, email, phone, acronym) VALUES ( %s, %s, %s, %s, %s );"
    __select_asn__ = "SELECT asname FROM ASN WHERE ASN = %d;"
    __select_organisation__ = "SELECT acronym FROM Organisation WHERE acronym = %s;"
    __select_iprange__ = "SELECT range FROM IPRanges WHERE range = %s;"
    __select_contact__ = "SELECT fname, lname FROM Contact WHERE fname = %s AND lname = %s;"

    def executeAndCommit(self, query, *values):
        '''
            Execute the query given in parameter and commit transaction
        '''
        cursor = self.connection.cursor()
        cursor.execute(query, values)
        self.connection.commit()

    def executeAndReturnResult(self, query, *values):
        cursor = self.connection.cursor()
        cursor.execute(query, values)
        try:
            result = cursor.fetchall()
            return result
        except:
            return None

    def containsRow(self, query, *values):
        result = self.executeAndReturnResult(query, *values)
        if result == None or len(result) <= 0:
            return False
        else:
            return True

    def containsASN(self, asn):
        return self.containsRow(self.__select_asn__, asn)

    def containsOrganisation(self, acronym):
        return self.containsRow(self.__select_organisation__, acronym)

    def containsIPRange(self, iprange):
        if type(iprange) == list: 
            for range in iprange:
                if self.containsRow(self.__select_iprange__, range):
                    return True
        elif type(iprange) == str:
            return self.containsRow(self.__select_iprange__, range)
        return False

    def containsContact(self, fname, lname):
        return self.containsRow(self.__select_contact__, fname, lname)

    def getAllInfoForIP(self, ip):
        result = self.executeAndReturnResult(__select_all__, ip)
        if result == None or len(result) <= 0:
            return False
        else:
            return result[0]

    def insertASN(self, asn, asname, country_code, email):
        self.executeAndCommit(self.__insert_asn__, asn, asname, country_code, email)

    def insertOrganisation(self, acronym, name, asn):
        self.executeAndCommit(self.__insert_organisation__, acronym, name, asn)

    def insertIPRange(self, range, asn, netname, acronym, email):
        self.executeAndCommit(self.__insert_ipranges__, range, asn, netname, acronym, email)

    def insertContact(self, fname, lname, email, phone, acronym):
        self.executeAndCommit(self.__insert_contact__, fname, lname, email, phone, acronym)

    def close(self):
        self.connection.close()

    def storeAbuseInfo(self, ai):
        """
            Complete the database with information stored in ai object.
            Only update ASN and IPRange tables.  Information stored in Contacts
            and Organisation are supposed to be manually imported from Belnet CRM
            or any other information source available for the CERT
        """
        if(not self.containsASN(ai.asn)): 
            self.insertASN(ai.asn, ai.asname, ai.countryCode, ai.abuseMail)

        if(not self.containsIPRange(ai.ipRange)):
            self.insertIPRange(ai.ipRange, ai.asn, ai.networkName, ai.acronym, ai.abuseMail)

        return True    

    def getAbuseInfo(self, ip):
        if(self.containsIPRange(ip)):
            infos = self.getAllInfoForIP(ip)
            [range, netname, asn, asname, email, country_code, country_name] = infos

            abuseinfo = {}
            abuseinfo["abuseMail"]   = email
            abuseinfo["countryCode"] = country_code
            abuseinfo["countryName"] = country_name
            abuseinfo["networkName"] = netname
            abuseinfo["ipRange"]     = range
            abuseinfo["customer"]    = acronym
            abuseinfo["asn"]         = asn
            abuseinfo["asname"]      = asname
            return AbuseInfo(abuseinfo)
        else:
            return None
            
            
class PostgreSQLWhois(SQLWhois):

    def __init__(self, srv, db, usr, pwd):
        self.connection = pgdb.connect(srv + ":" + db + ":" + usr + ":" + pwd)            
