import commands
import re
import time
import string
import pgdb

from abusehelper.contrib.enrichment.abuseinfo import AbuseInfo
from abusehelper.contrib.enrichment.cymruwhois import CymruWhois
from abusehelper.contrib.enrichment.customwhois import CustomWhois
from abusehelper.contrib.enrichment.sqlwhois import SQLWhois, PostgreSQLWhois

class EventEnrichment():
    pgsql = None
    cymru = None
    cwhois = None    
    
    def __init__(self, dbsrv=None, dbname=None, dbusr=None, dbpwd=None):
        self.cymru = CymruWhois()
        self.cwhois = CustomWhois()        
        if(dbsrv is not None and dbname  is not None and dbusr is not None and dbpwd is not None):
            self.pgsql = PostgreSQLWhois(dbsrv, dbname, dbusr, dbpwd)
        return
 
    def lookup(self, ip):
        abuseInfo = None
        #try to query SQL backend
        #if something go wrong, let query other sources
        if(self.pgsql is not None):
            try:
                abuseInfo = self.__sqlSearchAbuseInfo(ip)
            except:
                abuseInfo = None


        if(abuseInfo is None):
            cwhoisAI = self.__cwhoisSearchAbuseInfo(ip)            
            cymruAI = self.__cymruSearchAbuseInfo(ip)            
            
            ai = self.__mergeAbuseInfo(cwhoisAI, cymruAI)
            ai = self.__checkAbuseInfo(ai)

            if(self.pgsql is not None):
                try:
                    self.__sqlAddAbuseInfo(ai)
                except:
                    return ai
            return ai


    def __sqlSearchAbuseInfo(self, ip):
        if(self.pgsql is not None):
            return self.pgsql.getAbuseInfo(ip)
        else:
            return None

    def __sqlAddAbuseInfo(self, ai):
        if(self.pgsql is not None):
            return self.pgsql.storeAbuseInfo(ai)
        else:
            return False

    def __cymruSearchAbuseInfo(self, ip):
        return self.cymru.getAbuseInfo(ip)
        
    def __cwhoisSearchAbuseInfo(self, ip):
        return self.cwhois.getAbuseInfo(ip)        

    def __compareAbuseInfo(self, info1, info2):
        info1Dic = info1.getAll()
        info2Dic = info2.getAll()   
        for k in info1Dic.keys():
            if info1Dic[k] is None and k in info2Dic and info2Dic[k] is not None:         
                info1Dic[k] = info2Dic[k]              
        return AbuseInfo(info1Dic)


    def __mergeAbuseInfo(self, info1, info2):
        res = None

        if (info1 is None):
            res = info2
        elif (info2 is None):
            res = info1
        else:
            res = self.__compareAbuseInfo(info1, info2)
                       
        return res
        
    def __checkAbuseInfo(self, info):    
        if info:
            infoDict = info.getAll()

            return AbuseInfo(infoDict)      
        else:
            return None
                  
 
# for compatibility with previous release   
def searchAbuseInfo(ip):
    eenrichment = EventEnrichment()
    return eenrichment.lookup(ip)
    
    
    
    
    
