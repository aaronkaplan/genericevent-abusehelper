import commands
import re
import time
import string
import pgdb

class AbuseInfo(object):

    # !! Use carrefully the possibility to set internal dict !!
    def __init__(self, dict=None):
        self._abuseinfo = {}

        if(dict is None):
            self._abuseinfo["abuseMail"]   = None
            self._abuseinfo["countryCode"] = None
            self._abuseinfo["countryName"] = None
            self._abuseinfo["sourceName"]  = None
            self._abuseinfo["networkName"] = None
            self._abuseinfo["networkInfo"] = None
            self._abuseinfo["ipRange"]     = None
            self._abuseinfo["customer"]    = None
            self._abuseinfo["asn"]         = None
            self._abuseinfo["asname"]      = None
            self._abuseinfo["acronym"]     = None
            self._abuseinfo["bgp"]         = None            
        else:
            self._abuseinfo = dict

    def __setKey(self, key, value):
        self._abuseinfo[key] = value

    def __getValue(self, key):
        if key in self._abuseinfo:
            return self._abuseinfo[key]
        else:
            return None 

    def getAbuseMail(self):
        return self.__getValue("abuseMail")

    def setAbuseMail(self, value):
        self.__setKey("abuseMail", value)

    def getCountryCode(self):
        return self.__getValue("countryCode")

    def setCountryCode(self, value):
        self.__setKey("countryCode", value)
    
    def getCountryName(self):
        return self.__getValue("countryName")

    def setCountryName(self, value):
        self.__setKey("countryName", value)

    def getSourceName(self):
        return self.__getValue("sourceName")

    def setSourceName(self, value):
        self.__setKey("sourceName", value)

    def getNetworkName(self):
        return self.__getValue("networkName")

    def setNetworkName(self, value):
        self.__setKey("networkName", value)

    def getNetworkInfo(self):
        return self.__getValue("networkInfo")

    def setNetworkInfo(self, value):
        self.__setKey("networkInfo", value)

    def getIPRange(self):
        return self.__getValue("ipRange")

    def setIPRange(self, value):
        self.__setKey("ipRange", value)

    def getCustomer(self):
        return self.__getValue("customer")

    def setCustomer(self, value):
        self.__setKey("customer", value)

    def getASN(self):
        return self.__getValue("asn")

    def setASN(self, value):
        self.__setKey("asn", value)

    def getASName(self):
        return self.__getValue("asname")

    def setASName(self, value):
        self.__setKey("asname", value)

    def getAcronym(self):
        return self.__getValue("acronym")

    def setAcronym(self, acronym):
        return self.__setValue("acronym", value)
        
    def getBGP(self):
        return self.__getValue("bgp")

    def setBGP(self, value):
        self.__setKey("bgp", value)   

    def getAll(self):
        return self._abuseinfo

    abuseMail = property(getAbuseMail, setAbuseMail, None, "abuseMail")
    countryCode = property(getCountryCode, setCountryCode, None, "countryCode")
    countryName = property(getCountryName, setCountryName, None, "countryName")
    sourceName = property(getSourceName, setSourceName, None, "sourceName")
    networkName = property(getNetworkName, setNetworkName, None, "networkName")
    networkInfo = property(getNetworkInfo, setNetworkInfo, None, "networkInfo")
    ipRange = property(getIPRange, setIPRange, None, "ipRange")
    customer = property(getCustomer, setCustomer, None, "customer")
    asn = property(getASN, setASN, None, "asn")
    asname = property(getASName, setASName, None, "asname")
    acronym = property(getAcronym, setAcronym, None, "acronym")
    bgp = property(getBGP, setBGP, None, "bgp")
    
