import commands
import re
import time
import string
import iptools # pip install iptools

from abusehelper.contrib.enrichment.abuseinfo import AbuseInfo


class CymruWhois():

    def queryWhois(self, ip):
        asn = None
        asname = None
        bgpprefix = None      
        countrycode = None    
    
        ip = string.split(ip, "/")[0] #remove the mask
        (status, output) = commands.getstatusoutput("whois -h whois.cymru.com -v " + ip)
               
        if status == 0:
            result = string.split(output, "\n")
            nresults = len(result)
            if(nresults > 1):
                header = result[nresults-2]
                content = result[nresults-1]
                
                infos = string.split(content, "|")
                
                if(len(infos) == 7):
                    [asn_aux, ip_aux, bgpprefix_aux, countrycode_aux, registry_aux, allocated_aux, asname_aux] = infos
                    asn = string.strip(asn_aux)
                    asname = string.strip(asname_aux)
                    bgpprefix = string.strip(bgpprefix_aux)
                    countrycode = string.strip(countrycode_aux)                                        
                    
                    if asn == "NA":
                        asn = None
                    if asname == "NA":
                        asname = None     
                    if countrycode == "NA" or countrycode == "":
                        countrycode = None
                    if bgpprefix == "NA":
                        bgpprefix = None
                    else:
                        try:
                            bgpprefix_aux = iptools.ipv4.cidr2block(bgpprefix)
                            bgpprefix = str(bgpprefix_aux[0]) + "-" + str(bgpprefix_aux[1])   
                        except:
                            '''placeholder'''   
                                                                
        return [asn, asname, bgpprefix, countrycode]

    def getAbuseInfo(self, ip):
        infos = self.queryWhois(ip)
        if infos is not None:
            [asn, asname, bgpprefix, countrycode] = infos
            ai = AbuseInfo() 
            ai.asn = asn
            ai.asname = asname
            ai.bgp = bgpprefix
            ai.countryCode = countrycode            
            return ai
        else:
            return None
