from abusehelper.contrib.enrichment.abuseinfo import AbuseInfo
import commands
import re
import time
import string
import pgdb
import urllib2
import iptools


class CustomWhois():
                
    """  Regular Expressions  """
    networkrange1_exp = '\"name\":\"inetnum\",\s*\"value\":\"(?P<IP1>([0-2]?\d{1,2}\.){3}([0-2]?\d{1,2}))\s+\-\s+(?P<IP2>([0-2]?\d{1,2}\.){3}([0-2]?\d{1,2}))\"'   
    networkrange2_exp = '.*\"name\":\"route\",\s*\"value\":\"(?P<IP1>([0-2]?\d{1,2}\.){3}([0-2]?\d{1,2})\/[1-3]?[0-9])\".*'                 
    asn_exp = '\"name\":\"origin\",\s*\"value\":\"AS(?P<ASN>([0-9]+))\"'    
        

    def parseNetworkRange(self, string):
        netblock = None

        m = re.search(self.networkrange1_exp, string)
        if m != None:
            if str(m.group('IP1')) != "0.0.0.0" and str(m.group('IP2')) != "255.255.255.255":
                netblock = [m.group('IP1'), m.group('IP2')]

        if netblock == None:
            matchs = re.findall(self.networkrange2_exp, string, re.M|re.I)
            if matchs:
                for item in matchs:
                    try:
                        aux = iptools.ipv4.cidr2block(str(item[0]))
                        netblock = ["".join(aux[0]), "".join(aux[1])]
                    except:
                        '''placeholder''' 
        return netblock
        

    def parseASN(self, string):
        m = re.search(self.asn_exp, string)
        if m != None:
            return str(m.group('ASN'))
        return None                  
        
                
    def parse(self, string, ip):
        ai = AbuseInfo()
        ai.ipRange = self.parseNetworkRange(string)
        ai.asn = self.parseASN(string)        
        return ai
        

    def get_url_info(self, url):
      req = urllib2.Request(url)
      req.add_header('Accept', 'application/json;q=0.9,*/*;q=0.8')
      try:
          resp = urllib2.urlopen(req)
          content = resp.read()
      except:
          content = ''
      return content
      
      
    def queryWhois(self, ip):
        url = 'http://apps.db.ripe.net/whois/grs-search?query-string=' + str(ip) + '&source=ripe-grs&source=apnic-grs&source=arin-grs&source=lacnic-grs&source=radb-grs&source=afrinic-grs'
        output = self.get_url_info(url)       
        abuseInfo = self.parse(output, ip)
        return abuseInfo


    def getAbuseInfo(self, ip):
        return self.queryWhois(ip)
