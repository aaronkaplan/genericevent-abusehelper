import socket
import urlparse


###  IP and Host information if it doesnt already exist
def enrich_basic_info(ip, host, url):     
    
    if ip is None:
        if host is None:
            if url:
                host = url2host(url)
                ip = host2ip(host)                          
        else:                                        
            ip = host2ip(host)                         
    else:
        if url:
            if host is None:
                host = url2host(url)
        else:
            if host is None:            
                host = ip2host(ip)
    
    return [ip, host]




### Host2IP  
def host2ip(host):
    try:
        if hasattr(socket, 'setdefaulttimeout'):
            socket.setdefaulttimeout(0.5)
        ipaddr = socket.gethostbyname(host)
        return ipaddr
    except socket.error, (value, message):
        return None
        
        
### IP2Host 
def ip2host(ip):
    try:
        if hasattr(socket, 'setdefaulttimeout'):
            socket.setdefaulttimeout(0.5)                                                                    
        host = socket.getfqdn(ip)
        return host
    except socket.error, (value, message):
        return None       
            
    
### URL2Host
def url2host(url):
    parsed = urlparse.urlparse(url)
    if parsed.scheme and parsed.hostname:
        return parsed.hostname
    else:
        return None
